require './tetrist'

describe 'Tetrist' do
  # This is the minimal effective solution given the constraints of the problem.
  # Will also work as an integration test for more involved approaches
  it "gives the right answer when it gets one row's worth of Os" do
    g = Game.new([:O,:O,:O,:O,:O])
    moves = g.run_game
    moves.should eq 'LLLL LL  RR RRRR '
  end

  it "gives the right answer when it gets two rows' worth of Os" do
    g = Game.new([:O,:O,:O,:O,:O, :O,:O,:O,:O,:O])
    moves = g.run_game
    moves.should eq 'LLLL LL  RR RRRR LLLL LL  RR RRRR '
  end
end

describe 'Board' do
  it "is empty when new" do
    b = Board.new
    b.empty?.should be_true
  end
  it "is non-empty after a move has been made" do
    b = Board.new
    b.left(2)
    b.empty?.should be_false
  end
end
