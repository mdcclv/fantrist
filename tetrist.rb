require 'matrix'
  class Piece
    @matrix = nil
    @@shapes = {
      O: [ [1,1,0,0],[1,1,0,0],[0,0,0,0],[0,0,0,0] ],
      I: [ [1,1,1,1],[0,0,0,0],[0,0,0,0],[0,0,0,0] ],
      S: [ [1,0,0,0],[1,1,0,0],[0,1,0,0],[0,0,0,0] ],
      Z: [ [1,1,0,0],[0,1,1,0],[0,0,0,0],[0,0,0,0] ],
      L: [ [1,1,1,0],[1,0,0,0],[0,0,0,0],[0,0,0,0] ],
      J: [ [1,0,0,0],[1,1,1,0],[0,0,0,0],[0,0,0,0] ],
      T: [ [1,1,1,0],[0,1,0,0],[0,0,0,0],[0,0,0,0] ]
    }

    def initialize(type)
      @matrix = Matrix[ @@shapes[type][0] ]
    end

  end

  class Board
    attr_accessor :empty, :control_codes
    alias :empty? :empty
    @matrix = nil

    def initialize(cols=10, rows=22)
      @empty = true
      @matrix = Matrix.zero(rows, cols)
      @control_codes = ''
    end

    def key(str)
      @control_codes += str
    end

    def left(count=1)
      count.times do
        self.key('L')
      end
    end

    def right(count=1)
      count.times do
        self.key('R')
      end
    end

    def drop
      self.key(' ')
    end


  end

  class Player
    @game = nil
    @board = nil
    @last_move = nil

    def initialize(game)
      @game = game
      @board = Board.new
    end
    def play(pieces)
      pieces.each do |p|
        move = self.find_place_for(Piece.new(p))
        self.do_move(move)
        if move == 4
          @board.empty = true
        end
      end
      @board.control_codes
    end

    def find_place_for(piece)
      if @board.empty?
        @board.empty = false
        # this also returns @last_move, and I don't think that's as expressive
        # as an explicit return would be
        @last_move = -4
      else
        @last_move += 2
      end
    end

    def do_move(move)
      if move < 0
        @board.left(move.abs)
      elsif move > 0
        @board.right(move)
      end
      @board.drop
    end


  end


  class Game
    @player = nil
    @sequence = nil
    @commands = ''

    def initialize(seq=:rand)
      @sequence = seq
      @player = Player.new(self)
    end

    def run_game
      if @sequence.kind_of? Enumerable
        @player.play(@sequence)
      elsif @sequence == :rand
        puts "Don't bother: we only have one shape for now"
      end
    end
  end


